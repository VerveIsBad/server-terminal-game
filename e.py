matrix_a = [ # define a matrix_a 
    [1,2,3],
    [4,5,6],
    [7,8,9],
]
result = [[0 for c in range(len(matrix_a))] for c in range(len(matrix_a[0]))]
# create a matrix_a of the same size of matrix_a a

for c in range(len(matrix_a)): # For collums in matrix_a 
    for r in range(len(matrix_a[0])): # For rows in matrix_a
        result[c][r] = matrix_a[r][c] # result[collum][row] = matrix_a[row][collums]

print('---------------')
for line in result:
    print(' '.join(map(str, line)))