import socket
import sys

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # socket object 

HOST = socket.gethostname()
PORT = 1234

s.bind((HOST, PORT)) # server
s.listen(5) # listen or connections

rooms = []

while True:
    client, address = s.accept() # accept socket connections from client
    print(F'Connection  from {address} has been established!')
    
    msg = client.recv(1024).decode('utf-8')
    
    if msg == 'stop':
        sys.exit('Stopping...')
    print(msg)
    
    client.close()
    print('connection closed')







